# install dependencies

```bash
	bash install_dependencies.sh
```

# start web interface

```bash
	cd app
	python3 main_web.py
```

# start standalone  API
```bash
	cd app
	uvicorn main:app --host 0.0.0.0 --port 7001 --reload --debug --workers 1	
```
