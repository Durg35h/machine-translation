FROM debian:bullseye-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        g++ \
        bzip2 \
        unzip \
        make \
        wget \
        git \
        python3 \
        python3-dev \
        python3-setuptools \
        python3-pip \
        python3-wheel \
        zlib1g-dev \
        patch \
        ca-certificates \
        xz-utils \
        libtool \
        pkg-config \
        libsndfile1 \
        ffmpeg \
        build-essential \
	make \
	zlib1g-dev \
	libatlas-base-dev liblapack-dev libblas-dev llvm \
    && rm -rf /var/lib/apt/lists/* 

COPY ./app /app

RUN pip3 install numpy scipy numba Cython llvmlite wget wheel setuptools unidecode flask nemo_toolkit['all'] uvicorn fastapi 


EXPOSE 7000
WORKDIR /app/
CMD ["sh", "-c", " python3 ./main_web.py]
