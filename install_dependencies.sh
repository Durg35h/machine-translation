sudo apt-get install -y --no-install-recommends \
        g++ \
        bzip2 \
        unzip \
        make \
        wget \
        git \
        python3 \
        python3-dev \
        python3-setuptools \
        python3-pip \
        python3-wheel \
        zlib1g-dev \
        patch \
        ca-certificates \
        xz-utils \
        libtool \
        pkg-config \
        libsndfile1 \
        ffmpeg \
        build-essential \
	make \
	zlib1g-dev \
	libatlas-base-dev liblapack-dev libblas-dev llvm

pip3 install numpy scipy numba Cython llvmlite wget wheel setuptools unidecode flask nemo_toolkit['all'] uvicorn fastapi
