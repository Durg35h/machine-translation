
from flask import Flask     # importing modules
from flask import render_template, request, redirect

import time
import os 
import string
import random
import os
import subprocess
import os.path
import json
import shutil
import wave
import sys
from nemo.collections.nlp.models import MTEncDecModel
import nemo.collections.nlp as nemo_nlp

#model_en_es = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/es/en_es_24x6.nemo").eval()
#model_es_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/es/es_en_24x6.nemo").eval()

#model_en_de = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/de/en_de_24x6.nemo").eval()
#model_de_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/de/de_en_24x6.nemo").eval()

#model_en_fr = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/fr/en_fr_24x6.nemo").eval()
#model_fr_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/fr/fr_en_24x6.nemo").eval()

#model_en_zh = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/zh/en_zh_24x6.nemo").eval()
#model_zh_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/zh/zh_en_24x6.nemo").eval()

#model_en_ru = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/ru/en_ru_24x6.nemo").eval()
#model_ru_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/ru/ru_en_24x6.nemo").eval()

model_en_es = MTEncDecModel.from_pretrained("nmt_en_es_transformer24x6")
model_es_en = MTEncDecModel.from_pretrained("nmt_es_en_transformer24x6")

model_en_de = MTEncDecModel.from_pretrained("nmt_en_de_transformer24x6")
model_de_en = MTEncDecModel.from_pretrained("nmt_de_en_transformer24x6")

model_en_fr = MTEncDecModel.from_pretrained("nmt_en_fr_transformer24x6")
model_fr_en = MTEncDecModel.from_pretrained("nmt_fr_en_transformer24x6")

model_en_zh = MTEncDecModel.from_pretrained("nmt_en_zh_transformer24x6")
model_zh_en = MTEncDecModel.from_pretrained("nmt_zh_en_transformer24x6")

model_en_ru = MTEncDecModel.from_pretrained("nmt_en_ru_transformer24x6")
model_ru_en = MTEncDecModel.from_pretrained("nmt_ru_en_transformer24x6")

import re

app = Flask(__name__)   # creating a webpage

@app.route("/", methods=["POST","GET"]) # providing route
def home():     # defining the page
    if request.method == "POST":    # if request already provided
        itext = request.form["ilan"]    # collect text and language input 
        ilang = request.form["language"]
        if itext.strip() == "":
            return render_template("index.html")
        elif ilang == "dlan":
            return render_template("index.html")
        # print("itext : ",itext);
        # print("ilang : ",ilang);
        translations = []
        if ilang == "en_es":
            translations = model_en_es.translate([itext], source_lang="en", target_lang="es")
        elif ilang == "es_en":
            translations = model_es_en.translate([itext], source_lang="es", target_lang="en")
        elif ilang == "en_de":
            translations = model_en_de.translate([itext], source_lang="en", target_lang="de")
        elif ilang == "de_en":
            translations = model_de_en.translate([itext], source_lang="de", target_lang="en")
        elif ilang == "en_fr":
            translations = model_en_fr.translate([itext], source_lang="en", target_lang="fr")
        elif ilang == "fr_en":
            translations = model_fr_en.translate([itext], source_lang="fr", target_lang="en")
        elif ilang == "en_zh":
            translations = model_en_zh.translate([itext], source_lang="en", target_lang="zh")
        elif ilang == "zh_en":
            translations = model_zh_en.translate([itext], source_lang="zh", target_lang="en")
        elif ilang == "en_ru":
            translations = model_en_ru.translate([itext], source_lang="en", target_lang="ru")
        elif ilang == "ru_en":
            translations = model_ru_en.translate([itext], source_lang="ru", target_lang="en")

        o = "Original Text: "   # defining strings
        t = "Translation: "

        if len(itext.strip().split()) == 1:
            s1 = set()
            for x in translations[0].strip().split():
                s1.add(x.lower())
            s2 = " ".join(s1)
            return render_template("index.html", m=s2, itext=itext, o=o, t=t) # render HTML template
        return render_template("index.html", m=translations[0], itext=itext, o=o, t=t) # render HTML template
    else: # for fresh loading of webpage
        return render_template("index.html")    # render HTML template



if __name__ == "__main__":  # for running the webpage
    app.run(host='0.0.0.0', port=7000)
