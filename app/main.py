import uvicorn
from fastapi import FastAPI, UploadFile, File, Request
import time
import os 
import string
import random
import os
import subprocess
import os.path
import json
import shutil
import wave
import sys
from fastapi.middleware.cors import CORSMiddleware
from nemo.collections.nlp.models import MTEncDecModel
import nemo.collections.nlp as nemo_nlp

#model_en_es = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/es/en_es_24x6.nemo").eval()
#model_es_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/es/es_en_24x6.nemo").eval()

#model_en_de = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/de/en_de_24x6.nemo").eval()
#model_de_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/de/de_en_24x6.nemo").eval()

#model_en_fr = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/fr/en_fr_24x6.nemo").eval()
#model_fr_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/fr/fr_en_24x6.nemo").eval()

#model_en_zh = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/zh/en_zh_24x6.nemo").eval()
#model_zh_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/zh/zh_en_24x6.nemo").eval()

#model_en_ru = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/ru/en_ru_24x6.nemo").eval()
#model_ru_en = nemo_nlp.models.machine_translation.MTEncDecModel.restore_from(restore_path="files/ru/ru_en_24x6.nemo").eval()

model_en_es = MTEncDecModel.from_pretrained("nmt_en_es_transformer24x6")
model_es_en = MTEncDecModel.from_pretrained("nmt_es_en_transformer24x6")

model_en_de = MTEncDecModel.from_pretrained("nmt_en_de_transformer24x6")
model_de_en = MTEncDecModel.from_pretrained("nmt_de_en_transformer24x6")

model_en_fr = MTEncDecModel.from_pretrained("nmt_en_fr_transformer24x6")
model_fr_en = MTEncDecModel.from_pretrained("nmt_fr_en_transformer24x6")

model_en_zh = MTEncDecModel.from_pretrained("nmt_en_zh_transformer24x6")
model_zh_en = MTEncDecModel.from_pretrained("nmt_zh_en_transformer24x6")

model_en_ru = MTEncDecModel.from_pretrained("nmt_en_ru_transformer24x6")
model_ru_en = MTEncDecModel.from_pretrained("nmt_ru_en_transformer24x6")

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)


supported_lang = ["en_es","es_en","en_de","de_en","en_fr","fr_en","en_zh","zh_en","en_ru","ru_en"]

@app.get("/translate/")
async def read_item(lang: str = "", text: str = ""):
    dic1 = {}
    #global model_en_es , model_es_en, model_en_de, model_de_en, model_en_fr, model_fr_en, model_en_zh, model_zh_en, model_en_ru, model_ru_en
    if lang not in supported_lang:
        dic1["error"] = "language not supported"
        return json.dumps(dic1)
    if len(text.strip()) == 0:
        dic1["error"] = "text can not be empty"
        return json.dumps(dic1)
    if lang == "en_es":
        translations = model_en_es.translate([text], source_lang="en", target_lang="es")
        dic1["result"] = translations[0]
    elif lang == "es_en":
        translations = model_es_en.translate([text], source_lang="es", target_lang="en")
        dic1["result"] = translations[0]
    elif lang == "en_de":
        translations = model_en_de.translate([text], source_lang="en", target_lang="de")
        dic1["result"] = translations[0]
    elif lang == "de_en":
        translations = model_de_en.translate([text], source_lang="de", target_lang="en")
        dic1["result"] = translations[0]
    elif lang == "en_fr":
        translations = model_en_fr.translate([text], source_lang="en", target_lang="fr")
        dic1["result"] = translations[0]
    elif lang == "fr_en":
        translations = model_fr_en.translate([text], source_lang="fr", target_lang="en")
        dic1["result"] = translations[0]
    elif lang == "en_zh":
        translations = model_en_zh.translate([text], source_lang="en", target_lang="zh")
        dic1["result"] = translations[0]
    elif lang == "zh_en":
        translations = model_zh_en.translate([text], source_lang="zh", target_lang="en")
        dic1["result"] = translations[0]
    elif lang == "en_ru":
        translations = model_en_ru.translate([text], source_lang="en", target_lang="ru")
        dic1["result"] = translations[0]
    elif lang == "ru_en":
        translations = model_ru_en.translate([text], source_lang="ru", target_lang="en")
        dic1["result"] = translations[0]
    return json.dumps(dic1,ensure_ascii=False)

#@app.api_route("/en_es/", methods=["GET", "POST", "DELETE"])
#async def receive_file(request: Request):
#    text = str(request.query_params).replace("text=","").replace("_"," ")
#    translations = model_en_es.translate([text], source_lang="en", target_lang="es")
#    return translations[0]

#@app.api_route("/es_en/", methods=["GET", "POST", "DELETE"])
#async def receive_file(request: Request):
#    text = str(request.query_params).replace("text=","").replace("_"," ")
#    translations = model_es_en.translate([text], source_lang="es", target_lang="en")
#    return translations[0]

if __name__ == "__main__":
    #global model_en_es , model_es_en, model_en_de, model_de_en, model_en_fr, model_fr_en, model_en_zh, model_zh_en, model_en_ru, model_ru_en
    #model_en_es = MTEncDecModel.from_pretrained("nmt_en_es_transformer24x6")
    #model_es_en = MTEncDecModel.from_pretrained("nmt_es_en_transformer24x6")
    #model_en_de = MTEncDecModel.from_pretrained("nmt_en_de_transformer24x6")
    #model_de_en = MTEncDecModel.from_pretrained("nmt_de_en_transformer24x6")
    #model_en_fr = MTEncDecModel.from_pretrained("nmt_en_fr_transformer24x6")
    #model_fr_en = MTEncDecModel.from_pretrained("nmt_fr_en_transformer24x6")
    #model_en_zh = MTEncDecModel.from_pretrained("nmt_en_zh_transformer24x6")
    #model_zh_en = MTEncDecModel.from_pretrained("nmt_zh_en_transformer24x6")
    #model_en_ru = MTEncDecModel.from_pretrained("nmt_en_ru_transformer24x6")
    #model_ru_en = MTEncDecModel.from_pretrained("nmt_ru_en_transformer24x6")
    uvicorn.run("main:app", port=7001, reload=True,host='0.0.0.0',workers=1)
